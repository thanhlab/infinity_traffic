// Fake api
export const centerMaps = {
  lat: 16.053423, lng: 108.227527
}

export const apiNodes = [
  {
    "id": "1",
    "name": "Nút giao thông số 01", // Tên vị trí
    "position": { // Tọa độ
      "lat": 16.0507611,
      "lng": 108.2093622
    },
    "alertLevel": "1", // Đây là cấp độ tắc đường, 1 là biểu hiện cho màu xanh (Không tắc), 2 là biểu hiện cho màu vàng (Có nguy cơ tắc), 3 là biểu hiện cho màu xanh (Đang tắc)
    "reason": "Phương tiện lưu thông đông"
  },
  {
    "id": "2",
    "name": "Nút giao thông số 02",
    "position": {
      "lat": 16.061312,
      "lng": 108.234402
    },
    "alertLevel": "2",
    "reason": "Phương tiện lưu thông đông"
  },
  {
    "id": "3",
    "name": "Nút giao thông số 03",
    "position": { // Tọa độ
      "lat": 16.053344,
      "lng": 108.236858
    },
    "alertLevel": "1",
    "reason": "Phương tiện lưu thông đông"
  },
  {
    "id": "4",
    "name": "Nút giao thông số 04",
    "position": {
      "lat": 16.049416,
      "lng": 108.222284
    },
    "alertLevel": "3",
    "reason": "Phương tiện lưu thông đông"
  }
]

export const hours = [
  {
    index: 0,
    icon: "mdi-alarm",
    text: "Present"
  },
  {
    index: 1,
    icon: "mdi-clock-fast",
    text: "Hour"
  },
  {
    index: 2,
    icon: "mdi-clock-fast",
    text: "Hours"
  },
  {
    index: 3,
    icon: "mdi-clock-fast",
    text: "Hours"
  },
  {
    index: 4,
    icon: "mdi-clock-fast",
    text: "Hours"
  }
]

export const nodeStatistic = [
  {
    id: "0",
    name: "Trường Chinh - Lê Đại Hành",
    timeTraffic: "12",
    timingFail: "17h - 18h",
    nextPrediction: "100%",
    timeline: [
      {
        time: "17h30 - 18h00",
        status: "Mật động lưu thông đông"
      },
      {
        time: "17h30 - 18h00",
        status: "Xảy ra tai nạn"
      }
    ]
  },
  {
    id: "1",
    name: "Nguyễn Văn Linh - Đường 2/9",
    timeTraffic: "12",
    timingFail: "17h - 18h",
    nextPrediction: "100%",
    timeline: [
      {
        time: "17h30 - 18h00",
        status: "Mật động lưu thông đông"
      },
      {
        time: "17h30 - 18h00",
        status: "Xảy ra tai nạn"
      }
    ]
  },
  {
    id: "2",
    name: "Võ Văn Kiệt - Ngô Quyền",
    timeTraffic: "12",
    timingFail: "17h - 18h",
    nextPrediction: "100%",
    timeline: [
      {
        time: "17h30 - 18h00",
        status: "Mật động lưu thông đông"
      },
      {
        time: "17h30 - 18h00",
        status: "Xảy ra tai nạn"
      }
    ]
  },
  {
    id: "3",
    name: "Xô Viết Nghệ Tĩnh - Đường 2/9",
    timeTraffic: "12",
    timingFail: "17h - 18h",
    nextPrediction: "100%",
    timeline: [
      {
        time: "17h30 - 18h00",
        status: "Mật động lưu thông đông"
      },
      {
        time: "17h30 - 18h00",
        status: "Xảy ra tai nạn"
      }
    ]
  }
]