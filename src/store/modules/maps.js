import maps from '../../utils/api/maps'
import axios from 'axios'
import { changeCase, getError } from '../../utils/helper'
export default {
  namespaced: true,
  state:{
    status: false,
    positionsOnMap: []
  },
  actions: {
    async getPositionOnTheMap(context, data) {
      try {
        const respone = await maps.getPositionOnTheMap(axios, data)
        context.commit('savePositionOnMap', respone.data.data)
        // console.log('changeCase(respone)', respone.data.data)
        return changeCase(respone)
      } catch(err) {
        context.commit('savePositionOnMap', ...context.state.positionsOnMap)
        return getError(err)
      }
    }
  },
  getters: {
    info(state){ return state.positionsOnMap }
  },
  mutations: {
    savePositionOnMap(state, data) {
      return state.positionsOnMap = data
    }
  },
}