import Vue from 'vue'
import Vuex from 'vuex'
import maps from './modules/maps'
Vue.use(Vuex)

export default {
  modules: {
    maps
  }
}